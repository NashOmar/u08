terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "2.9.7"
    }
  }
}

provider "linode" {
  token = var.linode_api_token
}

resource "linode_instance" "instance1" {
  label            = "caprover1"
  type             = "g6-nanode-1"
  region           = "se-sto"
  authorized_keys  = [var.ssh_key]
  image            = "linode/ubuntu22.04"
  private_ip       = true
}

resource "linode_instance" "instance2" {
  label            = "caprover2"
  type             = "g6-nanode-1"
  region           = "se-sto"
  authorized_keys  = [var.ssh_key]
  image            = "linode/ubuntu22.04"
  private_ip       = true
}




